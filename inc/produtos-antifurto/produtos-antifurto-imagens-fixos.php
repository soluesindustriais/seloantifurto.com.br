<div class="grid"><div class="col-6"><div class="picture-legend picture-center"><a href="<?=$url?>imagens/produtos-antifurto/produtos-antifurto-01.jpg" class="lightbox" title="<?=$h1?>" target="_blank">

<picture>
  	<source type="image/webp" srcset="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-01.webp">
	<source type="image/jpeg" srcset="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-01.jpg">
    <img src="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-01.jpg" alt="<?=$h1?>" title="<?=$h1?>" />
</picture> 

</a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div><div class="col-6"><div class="picture-legend picture-center"><a href="<?=$url?>imagens/produtos-antifurto/produtos-antifurto-02.jpg" class="lightbox" title="<?=$h1?>" target="_blank">

<picture>
  	<source type="image/webp" srcset="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-02.webp">
	<source type="image/jpeg" srcset="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-02.jpg">
    <img src="<?=$url?>imagens/produtos-antifurto/thumbs/produtos-antifurto-02.jpg" alt="<?=$h1?>" title="<?=$h1?>" />
</picture> 

</a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div></div>