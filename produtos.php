<?
$h1 = "Produtos";
$title = "Produtos";
$desc = "Encontre diversos fornecedores de " . ucwords(str_replace("-", " ", $categorias[0])) . ", cote agora mesmo!";
$var = "Produtos";
include('inc/head.php');
include('inc/categoriasGeral.php');
?>
</head>


<style>
    @media only screen and (max-width: 764px) {
        #myDiv {
            display: none;
        }
    }
</style>

<body>
    <? include('inc/topo.php'); ?>
    <div class="wrapper">
        <main>
            <div class="content">
                <?= $caminho ?>

                <h1>Produtos</h1>
                <article class="full">
                    <p>Encontre o melhor fornecedor de <?= ucwords(str_replace("-", " ", $categorias[0])); ?> agora
                        mesmo!</p>
                    <ul class="thumbnails-main">

                        <?


						foreach ($categorias as $categoria) {
							$categoriaSemAcento = trataAcentos($categoria);
							$categoriaEspaco = str_replace("-", " ", $categoria);
							$categoriaEspacoUpper = mb_strtoupper($categoriaEspaco);
							echo "<li>
           			<a rel=\"nofollow\" href=\"$url" . $categoriaSemAcento . "-categoria\" title=\"$categoriaEspacoUpper\">
              		<img src=\"$url" . "imagens/$categoriaSemAcento/$categoriaSemAcento-1.jpg\" alt=\"$categoriaEspacoUpper\" title=\"$categoriaEspacoUpper\"/>
        		  	</a>
            		<h2>
            		<a href=\"$url" . $categoriaSemAcento . "-categoria\" title=\"$categoriaEspacoUpper\"/>$categoriaEspaco</a>
            		</h2>
					</li>";
						}


						?>

                    </ul>
                </article>
            </div>
        </main>
    </div>
    <div style=" position: absolute; bottom: 0; width: 100%;" id="myDiv">
        <? include('inc/footer.php');?>
    </div>
</body>

</html>