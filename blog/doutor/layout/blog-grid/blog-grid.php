<div id="blog" class="blog-grid">
	<?php
	$categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE); ?>
	<section itemid="<?= RAIZ; ?>/blog" itemscope itemtype="http://schema.org/LiveBlogPosting">
		<div class="bread bread--default">
			<div class="wrapper">
				<div class="bread__row">
					<?php Check::SetBreadcrumb($arrBreadcrump); ?>
					<h1 class="bread__title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<?php
			if (!$isMobile) :
				include('doutor/layout/blog-grid/aside-sig-grid.php');
			endif;
			?>
			<div class="article-container">

				<?php

				$Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
				$authors = $Read->getResult();

				$categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
				if (!$categ) : require 'doutor/layout/blog-grid/blog-inc-grid.php';
				else :
					$Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "stats=2&parent={$categ}");
					if (!$Read->getResult()) :
						$Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "stats=2&parent={$categ}");
						if (!$Read->getResult()) :
							WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
						else :
							$category = $Read->getResult();
							$category = $category[0]; ?>
							<!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO DA CATEGORIA  -->
							<?php if (CAT_CONTENT) : ?>
								<div class="category-content">
									<?= $category['cat_content']; ?>
								</div>
						<? endif;
						endif; ?>
						<div class="container pt-0">
							<div class="grid grid-col-2">
								<?php
								$Read->ExeRead(TB_BLOG, "WHERE cat_parent = :cat AND blog_status = :stats ORDER BY blog_date DESC LIMIT :limit OFFSET :offset", "cat={$categ}&stats=2&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");

								if ($Read->getResult()) :
									foreach ($Read->getResult() as $blog) :
										extract($blog); ?>
										<div class="blog-card">
											<div class="blog-card__image">
												<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
													<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $blog_cover ?>" alt="<?= $blog_title ?>" title="<?= $blog_title ?>">
												</a>
											</div>

											<div class="blog-card__info">
												<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>"><?= $blog_title ?></a></h3>
												<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
												$blogDay = $newDate[0];
												$blogMonth = $newDate[1];
												$blogYear = $newDate[2];
												$blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
												?>
												<p class="blog-card__date"><?= $blogFullDate ?></p>
												<div class="blog-card__author">
													<?php
													$authorKey = array_search($user_id, array_column($authors, 'user_id'));
													$authorName = $authors[$authorKey]['user_name'];
													?>
													Por:
													<a href="<?= $url ?>autor/<?= urlencode($authorName) ?>" rel="nofollow" title="<?= $authorName ?>"><?= $authorName ?></a>
												</div>

												<div class="blog-card__description">
													<?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) : ?>
														<p class="blog-card__content-text"><?= $blog_brevedescription ?></p>
													<?php else : ?>
														<p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
								<? endforeach;
								endif; ?>
							</div>
						</div>
					<? else : ?>
						<div class="container pt-0">
							<div class="grid-col-2">
								<?php
								foreach ($Read->getResult() as $cat) :
									extract($cat); ?>
									<div class="blog-card blog-card--category">
										<div class="blog-card__image">
											<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
												<?php
												if (empty($cat_cover)) : ?>
													<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/images/default.png" alt="<?= $cat_title ?>" title="<?= $cat_title ?>">
												<? else : ?>
													<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $cat_cover ?>" alt="<?= $cat_title ?>" title="<?= $cat_title ?>">
												<? endif; ?>
											</a>
										</div>
										<div class="blog-card__info">
											<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>"><?= $cat_title ?></a></h3>
											<div class="blog-card__description">
												<p><?= $cat_description; ?></p>
											</div>
										</div>
									</div>
									<?php endforeach;
								$Read->ExeRead(TB_BLOG, "WHERE cat_parent = :cat AND blog_status = :stats ORDER BY blog_date DESC LIMIT :limit OFFSET :offset", "cat={$categ}&stats=2&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
								if ($Read->getResult()) :
									foreach ($Read->getResult() as $blog) :
										extract($blog); ?>
										<div class="blog-card">
											<div class="blog-card__image">
												<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
													<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $blog_cover ?>" alt="<?= $blog_title ?>" title="<?= $blog_title ?>">
												</a>
											</div>

											<div class="blog-card__info">
												<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>"><?= $blog_title ?></a></h3>
												<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
												$blogDay = $newDate[0];
												$blogMonth = $newDate[1];
												$blogYear = $newDate[2];
												$blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
												?>
												<p class="blog-card__date"><?= $blogFullDate ?></p>
												<div class="blog-card__author">
													<?php
													$authorKey = array_search($user_id, array_column($authors, 'user_id'));
													$authorName = $authors[$authorKey]['user_name'];
													?>
													Por:
													<a href="<?= $url ?>autor/<?= urlencode($authorName) ?>" rel="nofollow" title="<?= $authorName ?>"><?= $authorName ?></a>
												</div>

												<div class="blog-card__description">
													<?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) : ?>
														<p class="blog-card__content-text"><?= $blog_brevedescription ?></p>
													<?php else : ?>
														<p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
								<? endforeach;
								endif; ?>
							</div>
						</div>
					<? endif; ?>

					<div class="blog-pagination d-flex justify-content-center">
						<?php
						$Pager->ExePaginator(TB_BLOG, "WHERE cat_parent = :cat AND blog_status = :stats ORDER BY blog_date DESC", "cat={$categ}&stats=2");
						echo $Pager->getPaginator();
						?>
					</div>
				<?php endif; ?>
			</div>
			<?php
			if ($isMobile) :
				include('doutor/layout/blog-grid/aside-sig-grid.php');
			endif;
			?>
		</div> <!-- wrapper -->
		<div class="clear"></div>
	</section>
</div>