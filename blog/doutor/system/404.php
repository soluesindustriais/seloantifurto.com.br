<!-- page content -->
<div class="col-md-12">
    <div class="col-middle">
        <div class="text-center text-center">
            <h1 class="error-number">404</h1>
            <h2>Desculpe, mas não foi possível encontrar esta página</h2>
            <p>Ela foi removida ou nunca existiu!</p>
            <div class="mid_center">
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
