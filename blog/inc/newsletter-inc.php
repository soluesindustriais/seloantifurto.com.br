<!--
<b>Cadastro de newslleter</b>
Sistema somente para captação de lista de e-mails, é possível exportar as listas em CSV
para outros sistemas.
Sistema de captação em ajax/real time. É enviado um e-mail para o cliente confirmar o cadastro,
e autententicar se o e-mail é verídico.
-->
<div class="container">
  <div class="wrapper">
    <form method="post" enctype="multipart/form-data" class="newsletter-form j_submit">
      <div class="j_load"></div>
      <h2>CADASTRE-SE E RECEBA NOSSAS NOVIDADES!</h2>
      <input type="hidden" name="action" value="GravaNewsletter"/>
      <input type="hidden" name="user_empresa" value="<?= EMPRESA_CLIENTE; ?>"/>
      <div class="row">
        <div class="col-5">
          <input placeholder="Nome" type="text" name="news_nome" required/>
        </div>
        <div class="col-5">
          <input placeholder="E-mail" type="text" name="news_email" required/>
        </div>
        <div class="col-2">
          <input type="submit" name="CadastraNews" value="Cadastrar"/>
        </div>
      </div>
    </form>
  </div>
  <div class="clear"></div>
</div>
